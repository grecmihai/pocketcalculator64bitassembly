.386
.model flat, stdcall
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;includem biblioteci, si declaram ce functii vrem sa importam
includelib msvcrt.lib
extern exit: proc
extern printf: proc
extern scanf: proc
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;declaram simbolul start ca public - de acolo incepe executia
public start
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;sectiunile programului, date, respectiv cod
.data
sir db 81 dup(0)
iesire db "Exit" , 0
text db "Introduceti o expresie:",13,10,0
linieNoua db 13,10,0
formatSir db "%s",0
formatNr db "%lli " , 0
rezultat dq 0
zeroAscii dd '0'
minusAscii dd '-'
egalAscii db '='
inmultireAscii db '*'
impartireAscii db '/'
adunareAscii db '+'
scadereAscii db '-'
TREI equ 3
zece dq 10
doi dq 2
bool dd 0
asciiTest dw 0
operanzi dq 0,0,0,0
lgVectOperanzi dd $-operanzi-8
operatii db 0,0,0
lgVectOperatii db 0
.code
;pt cele 4 operatii , in ECX se afla adresa catre primul operand , in EDX adresa catre cel de'al doilea operand
;fiind folosita conventia fastcall ; rezultatul este returnat in primul operand
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
adunare proc
finit
fild qword ptr[edx]
fild qword ptr[ecx]
fadd st(0) , st(1)
fistp qword ptr [ecx]
fwait
ret
adunare endp
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
scadere proc
finit
fild qword ptr[edx]
fild qword ptr[ecx]
fsub st(0) , st(1)
fistp qword ptr [ecx]
fwait
ret
scadere endp
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
inmultire proc
finit
fild qword ptr [edx]
fild qword ptr [ecx]
fmul st(0) , st(1)
fistp qword ptr [ecx]
fwait
ret
inmultire endp
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
impartire proc
finit
fild qword ptr [edx]
fild qword ptr [ecx]
fdiv st(0) , st(1)
fistp qword ptr [ecx]
fwait
ret
impartire endp
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
construireNumar proc;construireNumar(int[] operatii , int[] operanzi , int inceput )-fastcall
;in ecx avem adresa de inceput a vectorului de operatii
;in edx avem adresa de inceput a vectorului de operanzi
;in [esp+4] avem adresa de incepput a sirului citit de la tastatura
;vedem daca nu cumva stringul incepe cu o operatie
	mov ebp , [esp + 4]
	mov ebx , zeroAscii
	cmp byte ptr [ebp] , bl
	jb constr
	jmp golireRez
;punem acea operatie in vectorul de operatii si ne mutam pe urmatoarea pozitie
constr:
	add edx , 8
	mov al , byte ptr [ebp]
	mov [ecx] , al
	inc ecx
	inc ebp
;in cazul inca care nu incepe cu operatie , golim rezultatul anterior pentru a putea pune aici primul element
golireRez:
	mov dword ptr [edx] , 0
	mov dword ptr [edx +4] , 0
;verificam daca are si semn dupa inmultire:DA , bool = 1 , incrementam ECX    NU bool = 0
verificareSemn:
	push ebx
	mov ebx , minusAscii
	cmp bl , byte ptr [ebp]
	pop ebx
	jne fal
	;bool = 1 si sarim peste semn
	push eax 
	mov eax , 1
	mov bool , eax
	pop eax
	inc ebp
	jmp construct
	;bol = 1 si trecem la construirea numarului
fal:
	push eax
	mov eax , 0
	mov bool , eax
	pop eax
;algoritm de construire a numarului
construct:
	mov eax , 0
	mov al , byte ptr [ebp]
	mov asciiTest , ax
	finit
	fild zece
	fild qword ptr [edx];operanzi[i] e la st(0)
	fmul st(0) , st(1);operanzi[i]*10
	fild asciiTest;incarcam codul ascii , operanzi[i] e la st(1)
	fadd st(1) , st(0);operanzi[i]*10+ascii
	fild zeroAscii;incarcam codul ascii al lui 0 , operanzi[i] e la st(2)
	fsub st(2) , st(0);numarul final
	fxch st(2);il mutam in st(0)
	fistp qword ptr [edx]
	fwait
	;verificam daca s'a ajuns la semn
	inc ebp
	mov ebx , zeroAscii
	cmp bl , byte ptr [ebp]
	jbe verificare;in cazul in care codul ascii e mai mare decat cel al semnelor verificam daca nu cumva avem semnul =
	;in acest moment , ebp este la adresa la care se afla un semn , iar in operanzi[i] este pus numarul corespunzator
	mov al , byte ptr [ebp]
	cmp al , egalAscii
	je final
	mov [ecx] , al

	mov lgVectOperatii , cl
	inc ecx
	push ebx
	mov ebx , 0
	cmp ebx , bool
	pop ebx
	jne inversare;verificare a semnului
continuare:
	
	add edx , 8
	inc ebp
	jmp verificareSemn
verificare:
;verificam daca e semnul = , iar daca e tratam si cazul in care ultimul operand e negativ 
	mov bl , egalAscii
	cmp byte ptr [ebp] , bl
	jb construct
	push ebx
	mov ebx , 0
	cmp ebx , bool
	pop ebx
	je final
	finit
	fild doi;in st(0) e 2
	fild qword ptr[edx];in st(0) e numarul 
	fmul st(0) , st(1);in st(0) e numarul*2
	fild qword ptr [edx];in st(0) e numarul , in st(1) e numarul*2
	fsub st(0) , st(1);in st(0) e -numarul
	fistp qword ptr [edx]
	fwait
	jmp final
inversare:
	finit
	fild doi;in st(0) e 2
	fild qword ptr[edx];in st(0) e numarul 
	fmul st(0) , st(1);in st(0) e numarul*2
	fild qword ptr [edx];in st(0) e numarul , in st(1) e numarul*2
	fsub st(0) , st(1);in st(0) e -numarul
	fistp qword ptr [edx]
	fwait
	jmp continuare
	;dupa construirea numarului negativ trecem mai departe si verificam daca urmatorul numar este negativ
	
final:
	
ret 4
construireNumar endp

xchOperanzi proc;xchOperanzi(int inceput , int sfarsit)
;functie de tip fastcall , in ecx vom urca adresa de inceput al sirului dupa care vom adauga multiplii de 8 astfel incat sa 
;ajungem la numarul de la care se incepe schimbul
;in edx e adresa de inceput + lungimea vectorului pentru a fi la ultimul numar
	cmp ecx , edx
	je finalos;in cazul in care suntem la ultimul operand , nu mai facem xgh , doar setam acel numar cu 0 dupa operatie
bucla:;algoritm de interschimare a 2 numere consecutive
	push dword ptr [ecx + 4]
	push dword ptr [ecx]
	push dword ptr [ecx + 12]
	push dword ptr [ecx + 8]
	
	pop dword ptr [ecx]
	pop dword ptr [ecx + 4]
	pop dword ptr [ecx + 8]
	pop dword ptr [ecx + 12]
	
	add ecx , 8
	cmp ecx , edx
	jb bucla
finalos:
;al doilea operand il setam ca fiind 0 , deoarece deja l'am folosit si nu mai avem nevoie de el
	mov dword ptr [ecx] , 0
	mov dword ptr [ecx+4] , 0
	ret
	xchOperanzi endp

xchOperatii proc
;se comporta la fel ca si xchOperanzi 
	cmp ecx , edx
	je zro
	push eax
	mov eax , [esi]
	add eax , 2 
	cmp eax , ecx
	pop eax
	je zro
bucla:
	ror word ptr [ecx] , 8
	
	inc ecx
	cmp ecx , edx
	jb bucla
zro:
	mov byte ptr [ecx] , 0

ret
xchOperatii endp

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
start:
;afisam mesajul de inceput
	push offset text
	call printf
	add esp , 4
;incarcam adresele in registrii
	lea ebp , sir
	lea edi , operanzi
	lea esi , operatii
;introducem expresia/exit
	push offset sir
	push offset formatSir
	call scanf
	add esp , 8

	;verificam daca s'a introdus o expresie sau Exit
	push esi
	push edi
	push ecx
	mov ecx , 4
	cld
	lea esi , sir
	lea edi , iesire
	repe cmpsb
	cmp ecx , 0
	je fin
	
	pop ecx
	pop edi
	pop esi
	
	
;ne construim vectorii de operanzi si operatii
	push ecx 
	push edx
	push eax
	mov ecx , esi
	mov edx , edi
	push ebp
	call construireNumar
	pop eax
	pop edx
	pop ecx
	
;prima data facem inmultirile si impartirile
	push esi
	push edi
	push eax
	push ebp
	mov eax , edi
	add eax , 8;eax trebuie sa fie cum un termen in fata lui edi pt a retine mereu al doilea operand
	mov ebp , 0;in ebp vom avea contorul cu care vom parcurge numerele
inm:;verificam daca e inmultire
	push edx
	mov dl , inmultireAscii
	cmp byte ptr [esi + ebp] , dl
	pop edx
	jne imp;daca nu e , verificam daca e impartire
	mov ecx , edi
	mov edx , eax
	call inmultire
;dupa efectuarea inmultirii trebuie sa interschimbam operanzii incepand cu cel de'al doilea operand din inmultire
	push ecx
	push edx
	mov ecx , edi
	add ecx , 8;adresa celui de'al doilea operand din inmultirea actuala
	lea edx , lgVectOperanzi;adresa de dupa vectorul de opreanzi
	sub edx , 8;adresa ultimului operand , cand ajungem aici nu mai trebuie facuta interschimbarea , ci golirea
	call xchOperanzi
	pop edx
	pop ecx
	
;facem partea de interschimbare a operatiilor pe aceeasi idee
	push ecx
	push edx
	mov ecx , esi
	add ecx , ebp
	lea edx , lgVectOperatii
	dec edx
	call xchOperatii
	pop edx 
	pop ecx

	sub eax , 8
	sub edi , 8
	dec esi
	jmp verificareOperatii1
;deoarece ne'am dus cu o pozitie spre stanga dupa efectuarea operatiei , trebuie sa revenim pe pozitiile initiale	
imp:;exact acelasi algoritm ca si la inmultire
	push edx
	mov dl , impartireAscii
	cmp byte ptr [esi + ebp] , dl
	pop edx
	jne verificareOperatii1
	mov ecx , edi
	mov edx , eax
	call impartire
	push ecx
	push edx
	mov ecx , edi
	add ecx , 8
	lea edx , lgVectOperanzi
	sub edx , 8
	call xchOperanzi
	pop edx
	pop ecx
	
	push ecx
	push edx
	mov ecx , esi
	add ecx , ebp
	lea edx , lgVectOperatii
	dec edx
	call xchOperatii
	pop edx 
	pop ecx

	sub eax , 8
	sub edi , 8
	dec esi

;se verifica daca s'a ajuns la finalul operatiilor , deoarece pana nu terminam cu aceste operatii nu putem trece la + si -
verificareOperatii1:
	add edi , 8
	add eax , 8
	inc ebp
	cmp ebp , TREI
	jb inm
	
	pop ebp
	pop eax
	pop edi
	pop esi
;partea de adunari si scadare - acelasi algoritm	
	push esi
	push edi
	push eax
	push ebp
	mov eax , edi
	add eax , 8
	mov ebp , 0
adu:
	push edx
	mov dl , adunareAscii
	cmp byte ptr [esi + ebp] , dl
	pop edx
	jne sca
	mov ecx , edi
	mov edx , eax
	call adunare
	push ecx
	push edx
	mov ecx , edi
	add ecx , 8
	lea edx , lgVectOperanzi
	sub edx , 8
	call xchOperanzi
	pop edx
	pop ecx
	
	push ecx
	push edx
	mov ecx , esi
	add ecx , ebp
	lea edx , lgVectOperatii
	dec edx
	call xchOperatii
	pop edx 
	pop ecx

	sub eax , 8
	sub edi , 8
	dec esi
	jmp verificareOperatii2

sca:
	push edx
	mov dl , scadereAscii
	cmp byte ptr [esi + ebp] , dl
	pop edx
	jne verificareOperatii2
	mov ecx , edi
	mov edx , eax
	call scadere
	push ecx
	push edx
	mov ecx , edi
	add ecx , 8
	lea edx , lgVectOperanzi
	sub edx , 8
	call xchOperanzi
	pop edx
	pop ecx
	
	push ecx
	push edx
	mov ecx , esi
	add ecx , ebp
	lea edx , lgVectOperatii
	dec edx
	call xchOperatii
	pop edx 
	pop ecx

	sub eax , 8
	sub edi , 8
	dec esi


verificareOperatii2:
	add edi , 8
	add eax , 8
	inc ebp
	cmp ebp , TREI
	jb adu
	
	pop ebp
	pop eax
	pop edi
	pop esi
;in acest moment , s'au efectuat toate operatiile
;se afiseaza rezultatul si se trece pe linie noua , dupa care se reia algoritmul pana la introducerea "Exit"	
	push dword ptr [edi + 4]
	push dword ptr [edi]
	push offset formatNr
	call printf
	add esp , 12
	push offset linieNoua
	push offset formatSir
	call printf
	add esp , 8
	
	jmp start
	
fin:
	push 0
	call exit
end start
	